import os
from os import system, listdir, getcwd
from os.path import isfile, isdir, dirname, join

search_dir = join(getcwd(), "designer")
print(f"Searching for .ui files in '{search_dir}'…")
directory_files = [file for file in listdir(search_dir) if isfile(join(search_dir, file))]
for file in directory_files:
    print(f"Found candidate: {file}")
uifiles = [file for file in directory_files if file[-3:] == '.ui']
for file in uifiles:
    source = join(search_dir, file)
    target_file = file[:-3] + ".py"
    target = join(getcwd(), "prononciation", "gui", "qt5", target_file)
    system(f'pyuic5 {source} -o {target}')

    target = join(getcwd(), "prononciation", "gui", "qt6", target_file)
    system(f'pyuic6 {source} -o {target}')
    print(f"Converted {source} to {target}")