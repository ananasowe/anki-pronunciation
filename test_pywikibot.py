import pywikibot

def search_word_in_wiktionary(word):
    site = pywikibot.Site("fr", "wiktionary")  # French Wiktionary
    search_results = site.search(word, total=2)
    
    results = []
    for result in search_results:
        results.append((result.title(), result.pageid))
    
    return results

def get_audio_links_from_page(title):
    site = pywikibot.Site("fr", "wiktionary")  # French Wiktionary
    page = pywikibot.Page(site, title=title)
    
    audio_links = []
    print(f"Page: {page}")
    for template in page.templatesWithParams():
        template_name = template[0].title(with_ns=False)
        template_params = template[1]
        # Serach for pronunciation files
        if template_name == "écouter":
            audio_file = ''
            audio_description = ''
            for template_param in template_params:
                if template_param.startswith('audio='):
                    audio_file = template_param.split('=')[1].strip()
                    break
                elif '=' not in template_param and not audio_description:
                    audio_description = template_param
            audio_links.append((audio_file, audio_description))
    
    return audio_links

def get_file_link_from_commons(audio_file):
    site = pywikibot.Site("commons", "commons")
    file_page = pywikibot.FilePage(site, title=audio_file)
    return file_page.get_file_url()
                                    
# Example usage
search_word = "chat"
results = search_word_in_wiktionary(search_word)

# Display results
for title, pageid in results:
    print(f"Title: {title}")
    print(f"Page ID: {pageid}")
    audio_links = get_audio_links_from_page(title)
    for audio_file, audio_description in audio_links:
        print(f"Audio File: {audio_file}")
        audio_url = get_file_link_from_commons(audio_file)
        print(f"Audio URL: {audio_url}")
        print(f"Audio description: {audio_description}")
    print("---------")
