from aqt.qt import *
from aqt.utils import tooltip, restoreGeom, saveGeom
if qtmajor == 5:
    from .gui.qt5 import searchwindow  # type: ignore  # noqa
elif qtmajor == 6:
    from .gui.qt6 import searchwindow # type: ignore  # noqa

class SearchWindow(QDialog):
    def __init__(self, parent):
        self.parent = parent
        QDialog.__init__(self, parent, Qt.WindowType.Window)
        self.dialog = searchwindow.Ui_Dialog()
        self.dialog.setupUi(self)
        self.setWindowTitle("Find pronounciation")
        restoreGeom(self, "addon_searchwindow_dialog")


class Search():
    def __init__(self, editor, parent_window, search_text):
        self.editor = editor
        self.parent_window = parent_window
        self.search_text = search_text
        self.openWindow()

    def openWindow(self):
        window = SearchWindow(self.parent_window)
        print("[anki-pronunciation] Opening the audio search window")
        if window.exec():
            print("[anki-pronunciation] Audio selected")