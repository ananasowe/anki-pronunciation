# import the main window object (mw) from aqt
from aqt import mw, gui_hooks
# import all of the Qt GUI library
from aqt.qt import *
from aqt.editor import *
from anki.hooks import addHook
from .configuration import *
from .search_window import *

addon_path = os.path.dirname(__file__)


def findPronunciation(editor):
    textSelection = editor.web.selectedText()
    Search(editor, editor.parentWindow, search_text=textSelection)


def setupEditorButtonsFilter(buttons, editor):
    keybindValue = getConfigurationValue('keybinds')['find_pronunciation']
    keybind = QKeySequence(keybindValue)
    keybindDescription = keybind.toString(QKeySequence.SequenceFormat.NativeText)
    if getConfigurationValue('keybinds')['find_pronunciation']:
        button = editor.addButton(
            os.path.join(addon_path, "icons", "text_to_speech.png"),
            "pronunciation",
            findPronunciation,
            tip="Find pronunciation ({})".format(keybindDescription),
            keys=keybindValue
        )
        buttons.append(button)
    return buttons


addHook("setupEditorButtons", setupEditorButtonsFilter)
