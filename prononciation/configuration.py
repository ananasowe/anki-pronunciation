from aqt import mw

def getConfigurationValue(key, default=False):
    conf = mw.addonManager.getConfig(__name__)
    if conf:
        return conf.get(key, default)
    else:
        return default
